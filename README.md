# Memoire Master


Vous trouverez ici un modèle Latex pour générer des mémoires de master (M1 et M2), réalisés pour l'Université Paris 8 mais facilement adaptables. Ces mémoires sont aux normes françaises (guillemets, espaces, etc.).

Il a été testé sur texlive 2022 sans aucun bug.

Les intitulés des chapitres, des parties, des sections, et la structure doivent être personnalisés...

Et je vous recommande de mettre des schémas autant que possible.
